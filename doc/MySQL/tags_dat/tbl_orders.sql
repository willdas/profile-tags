-- 订单数据表： tbl_orders
-- 电商网站中用户购买物品下单的订单数据，总共112个字段，记录每个订单详细信息。
-- 120125条

CREATE TABLE `tbl_orders` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`siteId` int(10) unsigned NOT NULL,
`isTest` tinyint(1) unsigned NOT NULL COMMENT '是否是测试订单',
`hasSync` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否已同步(临时添加)',
`isBackend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为后台添加的订单',
`isBook` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为后台添加的订单',
`isCod` tinyint(1) unsigned NOT NULL COMMENT '是否是货到付款订单',
`notAutoConfirm` tinyint(1) unsigned NOT NULL COMMENT '是否是非自动确认订单',
`isPackage` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为套装订单',
`packageId` int(10) unsigned NOT NULL COMMENT '套装ID',
`orderSn` varchar(50) NOT NULL COMMENT '订单号',
`relationOrderSn` varchar(50) NOT NULL COMMENT '关联订单编号',
`memberId` int(10) unsigned NOT NULL COMMENT '会员id',
`predictId` int(10) unsigned NOT NULL COMMENT '会员购买预测ID',
`memberEmail` varchar(120) NOT NULL COMMENT '会员邮件',
`addTime` int(10) unsigned NOT NULL,
`syncTime` int(10) unsigned NOT NULL COMMENT '同步到此表中的时间',
`orderStatus` smallint(3) NOT NULL COMMENT '订单状态',
`payTime` int(10) unsigned NOT NULL COMMENT '在线付款时间',
`paymentStatus` smallint(3) unsigned NOT NULL COMMENT '付款状态：0 买家未付款 1 买家已付款 ',
`receiptConsignee` varchar(20) NOT NULL COMMENT '发票收件人',
`receiptAddress` varchar(255) NOT NULL COMMENT '发票地址',
`receiptZipcode` varchar(20) NOT NULL COMMENT '发票邮编',
`receiptMobile` varchar(30) NOT NULL COMMENT '发票联系电话',
`productAmount` decimal(10,2) unsigned NOT NULL COMMENT '商品金额，等于订单中所有的商品的单价乘以数量之和',
`orderAmount` decimal(10,2) unsigned NOT NULL COMMENT '订单总金额，等于商品总金额＋运费',
`paidBalance` decimal(10,2) unsigned NOT NULL COMMENT '余额账户支付总金额',
`giftCardAmount` decimal(10,2) unsigned NOT NULL COMMENT '礼品卡抵用金额',
`paidAmount` decimal(10,2) unsigned NOT NULL COMMENT '已支付金额',
`shippingAmount` decimal(10,2) NOT NULL COMMENT '淘宝运费',
`totalEsAmount` decimal(10,2) unsigned NOT NULL COMMENT '网单中总的节能补贴金额之和',
`usedCustomerBalanceAmount` decimal(10,2) unsigned NOT NULL COMMENT '使用的客户的余额支付金额',
`customerId` int(10) unsigned NOT NULL COMMENT '用余额支付的客户ID',
`bestShippingTime` varchar(100) NOT NULL COMMENT '最佳配送时间描述',
`paymentCode` varchar(20) NOT NULL COMMENT '支付方式code',
`payBankCode` varchar(20) NOT NULL COMMENT '网银代码',
`paymentName` varchar(60) NOT NULL COMMENT '支付方式名称',
`consignee` varchar(60) NOT NULL COMMENT '收货人',
`originRegionName` varchar(255) NOT NULL COMMENT '原淘宝收货地址信息',
`originAddress` varchar(255) NOT NULL COMMENT '原淘宝收货人详细收货信息',
`province` int(10) unsigned NOT NULL COMMENT '收货地址中国省份',
`city` int(10) unsigned NOT NULL COMMENT '收货地址中的城市',
`region` int(10) unsigned NOT NULL COMMENT '收货地址中城市中的区',
`street` int(10) unsigned NOT NULL COMMENT '街道ID',
`markBuilding` int(10) NOT NULL COMMENT '标志建筑物',
`poiId` varchar(64) NOT NULL DEFAULT '' COMMENT '标建ID',
`poiName` varchar(100) DEFAULT '' COMMENT '标建名称',
`regionName` varchar(200) NOT NULL COMMENT '地区名称（如：北京 北京 昌平区 兴寿镇）',
`address` varchar(255) NOT NULL COMMENT '收货地址中用户输入的地址，一般是区以下的详细地址',
`zipcode` varchar(20) NOT NULL COMMENT '收货地址中的邮编',
`mobile` varchar(15) NOT NULL COMMENT '收货人手机号',
`phone` varchar(20) NOT NULL COMMENT '收货人固定电话号',
`receiptInfo` text NOT NULL COMMENT '发票信息，序列化数组array(''title''=>.., ''receiptType'' =>..,''needReceipt'' => ..,''companyName''=>..,''taxSpotNum''=>..,''regAddress''=>..,''regPhone''=>..,''bank''=>..,''bankAccount''=>..)',
`delayShipTime` int(10) unsigned NOT NULL COMMENT '延迟发货日期',
`remark` text NOT NULL COMMENT '订单备注',
`bankCode` varchar(255) DEFAULT NULL COMMENT '银行代码,用于银行直链支付',
`agent` varchar(255) DEFAULT NULL COMMENT '处理人',
`confirmTime` int(11) DEFAULT NULL COMMENT '确认时间',
`firstConfirmTime` int(10) unsigned NOT NULL COMMENT '首次确认时间',
`firstConfirmPerson` varchar(200) NOT NULL COMMENT '第一次确认人',
`finishTime` int(11) DEFAULT NULL COMMENT '订单完成时间',
`tradeSn` varchar(255) DEFAULT NULL COMMENT '在线支付交易流水号',
`signCode` varchar(20) NOT NULL COMMENT '收货确认码',
`source` varchar(30) NOT NULL COMMENT '订单来源',
`sourceOrderSn` varchar(60) NOT NULL COMMENT '外部订单号',
`onedayLimit` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否支持24小时限时达',
`logisticsManner` int(1) NOT NULL COMMENT '物流评价',
`afterSaleManner` int(1) NOT NULL COMMENT '售后评价',
`personManner` int(1) NOT NULL COMMENT '人员态度',
`visitRemark` varchar(400) NOT NULL COMMENT '回访备注',
`visitTime` int(11) NOT NULL COMMENT '回访时间',
`visitPerson` varchar(20) NOT NULL COMMENT '回访人',
`sellPeople` varchar(20) NOT NULL COMMENT '销售代表',
`sellPeopleManner` int(1) NOT NULL COMMENT '销售代表服务态度',
`orderType` tinyint(2) NOT NULL COMMENT '订单类型 默认0 团购预付款1 团购正式单2',
`hasReadTaobaoOrderComment` tinyint(1) unsigned NOT NULL COMMENT '是否已读取过淘宝订单评论',
`memberInvoiceId` int(10) unsigned NOT NULL COMMENT '订单发票ID,MemberInvoices表的主键',
`taobaoGroupId` int(10) unsigned NOT NULL COMMENT '淘宝万人团活动ID',
`tradeType` varchar(100) NOT NULL COMMENT '交易类型,值参考淘宝',
`stepTradeStatus` varchar(100) NOT NULL COMMENT '分阶段付款的订单状态,值参考淘宝',
`stepPaidFee` decimal(10,2) NOT NULL COMMENT '分阶段付款的已付金额',
`depositAmount` decimal(10,2) unsigned NOT NULL COMMENT '定金应付金额',
`balanceAmount` decimal(10,2) unsigned NOT NULL COMMENT '尾款应付金额',
`autoCancelDays` int(10) unsigned NOT NULL COMMENT '未付款过期的天数',
`isNoLimitStockOrder` tinyint(1) unsigned NOT NULL COMMENT '是否是无库存限制订单',
`ccbOrderReceivedLogId` int(10) unsigned NOT NULL COMMENT '建行订单接收日志ID',
`ip` varchar(50) NOT NULL COMMENT '订单来源IP,针对商城前台订单',
`isGiftCardOrder` tinyint(1) unsigned NOT NULL COMMENT '是否为礼品卡订单',
`giftCardDownloadPassword` varchar(200) NOT NULL COMMENT '礼品卡下载密码',
`giftCardFindMobile` varchar(20) NOT NULL COMMENT '礼品卡密码找回手机号',
`autoConfirmNum` int(10) unsigned NOT NULL COMMENT '已自动确认的次数',
`codConfirmPerson` varchar(100) NOT NULL COMMENT '货到付款确认人',
`codConfirmTime` int(11) NOT NULL COMMENT '货到付款确认时间',
`codConfirmRemark` varchar(255) NOT NULL COMMENT '货到付款确认备注',
`codConfirmState` tinyint(1) unsigned NOT NULL COMMENT '货到侍确认状态0无需未确认,1待确认,2确认通过可以发货,3确认无效,订单可以取消',
`paymentNoticeUrl` text NOT NULL COMMENT '付款结果通知URL',
`addressLon` decimal(9,6) NOT NULL COMMENT '地址经度',
`addressLat` decimal(9,6) NOT NULL COMMENT '地址纬度',
`smConfirmStatus` tinyint(4) NOT NULL COMMENT '标建确认状态。1 = 初始状态；2= 已发HP，等待确认；3 = 待人工处理；4 = 待自动处理；5 = 已确认',
`smConfirmTime` int(10) NOT NULL COMMENT '请求发送HP时间，格式为时间戳',
`smManualTime` int(10) DEFAULT '0' COMMENT '转人工确认时间',
`smManualRemark` varchar(200) DEFAULT '' COMMENT '转人工确认备注',
`isTogether` tinyint(3) unsigned NOT NULL COMMENT '货票通行',
`isNotConfirm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是无需确认的订单',
`tailPayTime` int(11) NOT NULL DEFAULT '0' COMMENT '尾款付款时间',
`points` int(11) DEFAULT '0' COMMENT '网单使用积分',
`modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
`channelId` tinyint(4) DEFAULT '0' COMMENT '区分EP和商城',
`isProduceDaily` int(2) NOT NULL DEFAULT '0' COMMENT '是否日日单(1:是，0:否)',
`couponCode` varchar(20) NOT NULL DEFAULT '' COMMENT '优惠码编码',
`couponCodeValue` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠码优惠金额',
`ckCode` varchar(200) NOT NULL DEFAULT '',
PRIMARY KEY (`id`),
UNIQUE KEY `ux_Orders_ordersn` (`orderSn`),
KEY `memberId` (`memberId`),
KEY `agent` (`agent`),
KEY `addTime` (`addTime`),
KEY `payTime` (`payTime`),
KEY `orderStatus` (`orderStatus`),
KEY `sourceOrderSn` (`sourceOrderSn`),
KEY `smConfirmStatus` (`smConfirmStatus`),
KEY `idx_orders_source_orderStatus_hasReadTaobaoOrderComment` (`source`,`orderStatus`,`hasReadTaobaoOrderComment`),
KEY `idx_order_mobile` (`mobile`),
KEY `idx_orders_codConfirmState` (`codConfirmState`),
KEY `modified` (`modified`),
KEY `tailPayTime` (`tailPayTime`),
KEY `ix_Orders_syncTime` (`syncTime`),
KEY `ix_Orders_relationOrderSn` (`relationOrderSn`),
KEY `ix_Orders_consignee` (`consignee`),
KEY `idx_firstConfirmTime` (`firstConfirmTime`),
KEY `ix_Orders_paymentStatus` (`paymentStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=120128 DEFAULT CHARSET=utf8;